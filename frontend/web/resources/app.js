Dropzone.autoDiscover = false;

var app = angular.module('openTrip', ['ui.router', 'rzModule', 'angularTrix', 
                'thatisuday.dropzone','ngResource','ngCookies']);

app.constant("TOKEN","token");
app.constant("BASE_URL","http://localhost/project/opentrip/api/web/apiV1");
app.constant("TOKEN","token");
app.constant("LOGIN_PATH","/login")
app.constant("USERNAME","username");
app.value("ACCESS",[]);


app.config(function($httpProvider,$stateProvider, $urlRouterProvider, dropzoneOpsProvider) {

    $httpProvider.interceptors.push('InterceptorsConfigService');

    dropzoneOpsProvider.setOptions({
        url: '/upload_1.php',
        acceptedFiles: 'image/jpeg, images/jpg, image/png',
        addRemoveLinks: true,
        dictDefaultMessage: 'Click to add or drop photos',
        dictRemoveFile: 'Remove photo',
        dictResponseError: 'Could not upload this photo'
    });

    $urlRouterProvider.otherwise('/');

    $stateProvider

    // HOME STATES AND NESTED VIEWS ========================================
        .state('home', {
            url: '/',
            templateUrl: './resources/components/home/index.html',
            controller: 'HomeCtrl'
        })
        .state('login', {
            url: '/login',
            templateUrl: './resources/components/login/index.html',
            controller: 'LoginCtrl'
        })
        .state('create-open-trip', {
            url: '/create-open-trip',
            templateUrl: './resources/components/open-trip/create/index.html',
            controller: 'CreateOpenTripCtrl'
        })

});

app.factory('InterceptorsConfigService',function(CookiesService,TOKEN){
    return {
        request : function($config){            
            $config.headers['Authorization'] = "Bearer "+CookiesService.get(TOKEN);
            return $config;
        }
    }
});

app.run(function($rootScope,$location,CookiesService,TOKEN,LOGIN_PATH,ACCESS){
    var url = $location.path().toLowerCase();
    $rootScope.$on('$locationChangeStart',function(){

        var isLoginPage = url.indexOf(LOGIN_PATH) == -1;

        if( isLoginPage ){            

            if( angular.isUndefined(CookiesService.get(TOKEN)) 
                || CookiesService.get(TOKEN) == null ){

                $location.path(LOGIN_PATH);
            }

            CookiesService.removeAll();
            //action for access filter
            //var isValidAccess = ACCESS.indexOf(url) == -1;
            // if( ACCESS.indexOf( url) == -1 ){
            //     $location.href = LOGIN_PATH;                
            // }
        }    
    })
});

app.factory('CookiesService',function($cookies){

    return {
        put : function(key,value){
            $cookies.put(key,value);
        },
        get : function(key){
            return $cookies.get(key);
        },
        remove : function(key){
            return $cookies.remove(key);
        },
        removeAll : function(){
            for( var key in $cookies.getAll() ){
                $cookies.remove(key);
            }
        }
    }

})

app.factory('LoaderService',function(){
    
    var data = {};

    
    return {
        decrease : function(key){
            if( typeof data[key] == 'undefined' ){
                data[key]  = 0;
            }

            if( data[key] != 0 )
                data[key]--;
        },
        increase : function(key){
            if( typeof data[key] == 'undefined' ){
                data[key]  = 0;
            }
            data[key]++;            
        },
        status : function(key){
            return data[key] == 0 || typeof data[key] == 'undefined' ? false : true;
        }
    }

})