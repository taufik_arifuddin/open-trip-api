app.controller('LoginCtrl', function($scope,AuthService,CookiesService,LoaderService,TOKEN,USERNAME,$location) {
    $scope.auth = {};
    $scope.loaderService = LoaderService;

    $scope.login = function() {
      $scope.msg = false;

      LoaderService.increase('login');
      AuthService.login().doLogin(null,$scope.auth,function(response){        
        LoaderService.decrease('login');                
        $scope.isError = response.isError;
        if( response.isError ){                    
          $scope.msg = response.message;
        }else{
          CookiesService.put(TOKEN,response.data.token);
          $location.path("/home");
        }
      });
    }
})
