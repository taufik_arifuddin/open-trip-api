app.service('AuthService', function($http,$resource,BASE_URL) {

	var login = function() {
		return $resource(BASE_URL+"/default/login",null,{
			doLogin : {
				method : "post"
			}
		});
	};

	return {
		login : login
	}

})