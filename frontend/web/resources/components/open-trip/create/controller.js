app.controller('CreateOpenTripCtrl', function($scope) {
    var events = ['trixInitialize', 'trixChange', 'trixSelectionChange', 'trixFocus', 'trixBlur', 'trixFileAccept', 'trixAttachmentAdd', 'trixAttachmentRemove'];

    for (var i = 0; i < events.length; i++) {
        $scope[events[i]] = function(e, editor) {
            console.info('Event type:', e.type);
        }
    };


    $scope.dzOptions = {
        paramName: 'photo',
        maxFilesize: '10'
    };

    $scope.dzCallbacks = {
        'addedfile': function(file) {
            console.info('File added from dropzone 1.', file);
        }
    };
})
