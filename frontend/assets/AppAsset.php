<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
		'resources/assets/css/style.css',
		'resources/vendor/bower/angularjs-slider/dist/rzslider.min.css',	
		'resources/vendor/bower/trix/dist/trix.css',
		'resources/vendor/bower/dropzone/dist/dropzone.css',
		'resources/vendor/bower/ng-dropzone/dist/ng-dropzone.min.css'	
    ];
    public $js = [
		'resources/vendor/bower/angular/angular.js',
		'resources/vendor/bower/angular-resource/angular-resource.min.js',
		'resources/vendor/bower/angular-cookies/angular-cookies.min.js',
		'resources/vendor/bower/angular-ui-router/release/angular-ui-router.min.js',
		'resources/vendor/bower/angularjs-slider/dist/rzslider.min.js',   
		'resources/vendor/bower/trix/dist/trix.js',	
		'resources/vendor/bower/dropzone/dist/dropzone.js',	
		'resources/assets/js/script.js',
		'resources/vendor/bower/ng-dropzone/dist/ng-dropzone.js',	
		'resources/vendor/bower/angular-trix/dist/angular-trix.min.js',

		'resources/app.js',
		'resources/components/home/controller.js',
		'resources/components/login/services.js',
		'resources/components/login/controller.js',
		'resources/components/open-trip/create/controller.js',
	];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
