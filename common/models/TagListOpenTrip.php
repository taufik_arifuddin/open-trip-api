<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tag_list_open_trip".
 *
 * @property integer $id
 * @property integer $id_user
 * @property integer $id_open_trip
 * @property string $created_on
 */
class TagListOpenTrip extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tag_list_open_trip';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user', 'id_open_trip'], 'required'],
            [['id_user', 'id_open_trip'], 'integer'],
            [['created_on'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'Id User',
            'id_open_trip' => 'Id Open Trip',
            'created_on' => 'Created On',
        ];
    }
}
