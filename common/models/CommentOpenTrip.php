<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "comment_open_trip".
 *
 * @property integer $id
 * @property integer $id_user
 * @property integer $id_open_trip
 * @property integer $comment
 * @property integer $created_on
 */
class CommentOpenTrip extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'comment_open_trip';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user', 'id_open_trip', 'comment', 'created_on'], 'required'],
            [['id_user', 'id_open_trip', 'comment', 'created_on'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'Id User',
            'id_open_trip' => 'Id Open Trip',
            'comment' => 'Comment',
            'created_on' => 'Created On',
        ];
    }
}
