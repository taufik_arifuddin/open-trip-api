<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "open_trip".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $gallery
 * @property string $address
 * @property string $city
 * @property string $country
 * @property string $start_date
 * @property string $end_date
 * @property integer $price
 * @property integer $down_payment
 * @property integer $quota
 * @property integer $remaining_quota
 * @property string $meeting_point
 * @property string $facilities
 * @property string $exclude
 * @property string $itinerary
 * @property string $personal_equipment
 * @property string $notes
 * @property integer $id_tag_list
 * @property string $created_on
 * @property string $modified_on
 */
class OpenTrip extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'open_trip';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'description', 'gallery', 'address', 'city', 'country', 'price', 'down_payment', 'quota', 'remaining_quota', 'meeting_point', 'facilities', 'exclude', 'itinerary', 'personal_equipment', 'notes', 'id_tag_list', 'created_on', 'modified_on'], 'required'],
            [['start_date', 'end_date', 'created_on', 'modified_on'], 'safe'],
            [['price', 'down_payment', 'quota', 'remaining_quota', 'id_tag_list'], 'integer'],
            [['name'], 'string', 'max' => 20],
            [['description', 'meeting_point', 'facilities', 'exclude', 'itinerary', 'personal_equipment', 'notes'], 'string', 'max' => 120],
            [['gallery', 'address'], 'string', 'max' => 50],
            [['city', 'country'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'gallery' => 'Gallery',
            'address' => 'Address',
            'city' => 'City',
            'country' => 'Country',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'price' => 'Price',
            'down_payment' => 'Down Payment',
            'quota' => 'Quota',
            'remaining_quota' => 'Remaining Quota',
            'meeting_point' => 'Meeting Point',
            'facilities' => 'Facilities',
            'exclude' => 'Exclude',
            'itinerary' => 'Itinerary',
            'personal_equipment' => 'Personal Equipment',
            'notes' => 'Notes',
            'id_tag_list' => 'Id Tag List',
            'created_on' => 'Created On',
            'modified_on' => 'Modified On',
        ];
    }
}
