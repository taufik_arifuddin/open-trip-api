<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tag_list_blog".
 *
 * @property integer $id
 * @property integer $id_user
 * @property integer $id_blog
 * @property string $name
 * @property string $created_on
 * @property integer $created_by
 */
class TagListBlog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tag_list_blog';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user', 'id_blog', 'name', 'created_by'], 'required'],
            [['id_user', 'id_blog', 'created_by'], 'integer'],
            [['created_on'], 'safe'],
            [['name'], 'string', 'max' => 120],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'Id User',
            'id_blog' => 'Id Blog',
            'name' => 'Name',
            'created_on' => 'Created On',
            'created_by' => 'Created By',
        ];
    }
}
