<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "mybook".
 *
 * @property integer $id
 * @property integer $id_user
 * @property integer $id_open_trip
 * @property string $created_on
 */
class Mybook extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mybook';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user', 'id_open_trip'], 'required'],
            [['id_user', 'id_open_trip'], 'integer'],
            [['created_on'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'Id User',
            'id_open_trip' => 'Id Open Trip',
            'created_on' => 'Created On',
        ];
    }
}
