<?php

namespace common\models;

class FileContentParser extends \yii\base\Component{

    public function init(){

        $fileGetContents = file_get_contents("php://input");
        if( isset($fileGetContents) ){
            $_POST = gettype($fileGetContents) == 'string' ? 
            json_decode($fileGetContents,true) : $fileGetContents;
        }
        parent::init();
    }

}