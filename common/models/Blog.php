<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "blog".
 *
 * @property integer $id
 * @property integer $id_user
 * @property integer $id_tag_list
 * @property string $blog
 * @property string $thumbnail
 * @property string $created_on
 * @property string $modified_on
 */
class Blog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'blog';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user', 'id_tag_list', 'blog', 'thumbnail'], 'required'],
            [['id_user', 'id_tag_list'], 'integer'],
            [['created_on', 'modified_on'], 'safe'],
            [['blog'], 'string', 'max' => 120],
            [['thumbnail'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'Id User',
            'id_tag_list' => 'Id Tag List',
            'blog' => 'Blog',
            'thumbnail' => 'Thumbnail',
            'created_on' => 'Created On',
            'modified_on' => 'Modified On',
        ];
    }
}
