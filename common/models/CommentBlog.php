<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "comment_blog".
 *
 * @property integer $id
 * @property integer $id_user
 * @property integer $id_blog
 * @property string $comment
 * @property string $created_on
 */
class CommentBlog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'comment_blog';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user', 'id_blog', 'comment'], 'required'],
            [['id_user', 'id_blog'], 'integer'],
            [['created_on'], 'safe'],
            [['comment'], 'string', 'max' => 120],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'Id User',
            'id_blog' => 'Id Blog',
            'comment' => 'Comment',
            'created_on' => 'Created On',
        ];
    }
}
