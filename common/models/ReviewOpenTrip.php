<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "review_open_trip".
 *
 * @property integer $id
 * @property integer $id_user
 * @property integer $id_open_trip
 * @property string $review
 * @property string $created_on
 */
class ReviewOpenTrip extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'review_open_trip';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user', 'id_open_trip', 'review'], 'required'],
            [['id_user', 'id_open_trip'], 'integer'],
            [['created_on'], 'safe'],
            [['review'], 'string', 'max' => 120],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'Id User',
            'id_open_trip' => 'Id Open Trip',
            'review' => 'Review',
            'created_on' => 'Created On',
        ];
    }
}
