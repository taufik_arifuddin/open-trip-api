<?php


namespace api\modules\v1\models;

class BaseResponse{

    var $errorCode,$message,$data,$isError;

    public function __construct( $errorCode = null, $message = null, $data = null, $isError = null ){

        if( !is_null($errorCode ) ){
            $this->setErrorCode($errorCode); 
        }

        if( !is_null($message) ){
            $this->setMessage($message);
        }

        if( !is_null($data) ){
            $this->setData($data);
        }

        if( !is_null($isError) ){
            $this->setIsError($isError);
        }
    }


    public function setErrorCode( $errorCode ){
        $this->errorCode = $errorCode;
    }

    public function setMessage( $message ){
        $this->message = $message;
    }

    public function setData( $data ){
        $this->data = $data;
    }

    public function setIsError( $isError  ){
        $this->isError = $isError;
    }

    public function getResponse(){
        return [
            'message' => $this->message,
            'data' => $this->data,
            'isError' => $this->isError,
            'errorCode' => $this->errorCode
        ];

    }

    public function toJSON(){
        return json_encode($this->getResponse(),JSON_PRETTY_PRINT);
    }

}