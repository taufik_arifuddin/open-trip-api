<?php

namespace api\modules\v1\controllers;

use Yii;
use yii\web\Controller;

use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;

use api\modules\v1\models\BaseResponse;

/**
 * Default controller for the `apiV1` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
//             'class' => HttpBasicAuth::className(),
             'class' => HttpBearerAuth::className()
        ];
        
        $behaviors['authenticator']['except'] = ['login','create-user'];
        
        return $behaviors;
    }

    public function actionCreateUser(){
        $obj = new \common\models\User();
        $obj->username="test";
        $obj->setPassword('123');
        $obj->email = "coba@gmail.com";
        $obj->name ="userTest";
        $obj->city="sby";
        $obj->country="indo";
        $obj->role="10";
        $obj->zip_code = "zipCode";
        $obj->address="asdasd";
        if ( !$obj->save() ){
            var_dump($obj->errors);
        }
    }

    public function actionIndex()
    {
        if( Yii::$app->user->isGuest ){
            die("You are not logged in");
        }else{
            die("You are logged in");
        }
    }

    public function actionLogin(){


        $post = Yii::$app->request->post();

        $model = new LoginForm();
        
        if(  isset($post['username']) ) 
            $model->username = $post['username'];
        if(  isset($post['password']) ) 
            $model->password = $post['password'];
        $response = new BaseResponse(null,"Username / password salah",null,true);   
        $token = $model->login();
        if ( $token ) {
            $response->setMessage('Anda berhasil login');
            $response->setIsError(false);            
            $response->setData($token);
        }
        
        echo $response->toJSON();
    }
    public function actionLogout(){

    }
}
