<?php

namespace api\modules\v1\controllers;

use yii\rest\ActiveController;

class TagListOpenTripController extends ActiveController{

    public $modelClass = 'common\models\TagListOpenTrip';

    public function behaviors()
    {
        $behaviors = parent::behaviors();
    
        // remove authentication filter
        $auth = $behaviors['authenticator'];
        unset($behaviors['authenticator']);
        
        // add CORS filter
        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
        ];
        
        // re-add authentication filter
        $behaviors['authenticator'] = [
            'class' => \yii\filters\auth\HttpBearerAuth::className()
        ];
        // avoid authentication on CORS-pre-flight requests (HTTP OPTIONS method)
//        $behaviors['authenticator']['except'] = ['options'];
    
        return $behaviors;
    }

    // public function actions()
    // {
    //     $actions = parent::actions();
    
    //     // disable the "delete" and "create" actions
    //     unset($actions['delete'], $actions['create']);
    
    
    //     return $actions;
    // }    


}