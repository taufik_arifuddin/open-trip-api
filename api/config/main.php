<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-api',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'api\controllers',
    'bootstrap' => ['log','FileGetContentParser'],
    'modules' => [
        'apiV1' => [
            'class' => 'api\modules\v1\Module',
        ],
    ],
    'components' => [
        'FileGetContentParser' => [
            'class' => 'common\models\FileContentParser'
        ],
        'request' => [
            'csrfParam' => '_csrf-api',
            'enableCookieValidation' => false,
            'enableCsrfValidation' => false
        ],
        'response' => [
            'format' => yii\web\Response::FORMAT_JSON,
            'charset' => 'UTF-8',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'identityCookie' => ['name' => '_identity-api', 'httpOnly' => true],
            'enableAutoLogin' => true,
            'enableSession' => false,
            'loginUrl' => null,            
        ],
        'session' => [
            // this is the name of the session cookie used for login on the api
            'name' => 'advanced-api',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],  
    ],    
    'params' => $params,
];
